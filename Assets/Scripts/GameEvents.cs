﻿using System;
using UnityEngine;

public class GameEvents : MonoBehaviour {
    public static GameEvents current;

    private void Awake() {
        current = this;
    }

    /*******************************************************/
    /*****          Golf Shot Events                  ****/ 
    /*******************************************************/
    public event Action onHoleComplete;
    public void HoleComplete() {
        if (onHoleComplete != null) {
            onHoleComplete();
        }
    }

    /*******************************************************/
    /*****          Club Select Events                ****/ 
    /*******************************************************/
    public event Action onClubChange;
    public void ClubChange() {
        if (onClubChange != null) {
            onClubChange();
        }
    }

    /*******************************************************/
    /*****          Shot State Transitions            ****/ 
    /*******************************************************/
    public event Action onInitiateShotTargeting;
    public void InitiateShotTargeting() {
        if (onInitiateShotTargeting != null) {
            onInitiateShotTargeting();
        }
    }

    public event Action onInitiateShotPrep;
    public void InitiateShotPrep() {
        if (onInitiateShotPrep != null) {
            onInitiateShotPrep();
        }
    }

    public event Action onInitiateShotPower;
    public void InitiateShotPower() {
        if (onInitiateShotPower != null) {
            onInitiateShotPower();
        }
    }
    public event Action onGetShotPower;
    public void GetShotPower() {
        if (onGetShotPower != null) {
            onGetShotPower();
        }
    }

    public event Action onInitiateShotAccuracy;
    public void InitiateShotAccuracy() {
        if (onInitiateShotAccuracy != null) {
            onInitiateShotAccuracy();
        }
    }
    public event Action onGetShotAccuracy;
    public void GetShotAccuracy() {
        if (onGetShotAccuracy != null) {
            onGetShotAccuracy();
        }
    }

    public event Action<float, float> onInitiateShot;
    public void InitiateShot(float swingPercent, float accuracy) {
        if (onInitiateShot != null) {
            onInitiateShot(swingPercent, accuracy);
        }
    }

    /*******************************************************/
    /*****          Debugging Events                  ****/ 
    /*******************************************************/
    public event Action onResetShot;
    public void ResetShot() {
        if (onResetShot != null) {
            onResetShot();
        }
    }
}