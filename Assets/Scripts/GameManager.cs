﻿using UnityEngine;
using Utilities;

public class GameManager : MonoBehaviour {
    private ShotState State;

    [SerializeField]
    private Club[] clubBag;
    private int currentClubIndex = 0;

    void Awake() {
        State = ShotState.Idle;
    }

    void PreviousClub() {
        currentClubIndex = (currentClubIndex - 1 + clubBag.Length) % clubBag.Length;
        GameEvents.current.ClubChange();
    }

    void NextClub() {
        currentClubIndex = (currentClubIndex + 1) % clubBag.Length;
        GameEvents.current.ClubChange();
    }

    public Club CurrentClub() {
        return clubBag[currentClubIndex];
    }

    void Update() {
        if (Input.GetButtonDown("Swing")) {
            switch (State) {
                case ShotState.Idle:
                    State = ShotState.Targeting;
                    GameEvents.current.InitiateShotTargeting();
                    break;
                case ShotState.Targeting:
                    State = ShotState.ShotPrep;
                    GameEvents.current.InitiateShotPrep();
                    break;
                case ShotState.ShotPrep:
                    State = ShotState.Power;
                    GameEvents.current.InitiateShotPower();
                    break;
                case ShotState.Power:
                    GameEvents.current.GetShotPower();
                    State = ShotState.Accuracy;
                    GameEvents.current.InitiateShotAccuracy();
                    break;
                case ShotState.Accuracy:
                    GameEvents.current.GetShotAccuracy();
                    State = ShotState.Idle;
                    break;
            }
        }

        if (Input.mouseScrollDelta.y > 0f) {
            NextClub();
        } else if (Input.mouseScrollDelta.y < 0f) {
            PreviousClub();
        }

        if (Input.GetButtonDown("Cancel")) {
            GameEvents.current.ResetShot();
        }
    }
}