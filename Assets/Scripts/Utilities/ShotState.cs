namespace Utilities {
  
  public enum ShotState {
    Idle,
    Targeting,
    ShotPrep,
    Power,
    Accuracy
  }

}
