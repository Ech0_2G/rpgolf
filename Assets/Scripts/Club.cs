﻿using UnityEngine;

[CreateAssetMenu(menuName="Clubs/Club")]
public class Club : ScriptableObject {
    public new string name;
    public Sprite icon;
    public int distance;
    public float launchAngle;
    public float launchLimits;
    public LayerMask validSurfaces;

    [Range(0f, 1f)]
    public float badSurfacePenalty;
}