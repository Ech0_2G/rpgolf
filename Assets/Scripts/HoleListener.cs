﻿using UnityEngine;

public class HoleListener : MonoBehaviour {

    void OnTriggerEnter(Collider other) {
        Debug.Log("\n===== Trigger!!! ===================================\n");
        GameEvents.current.HoleComplete();
    }
}