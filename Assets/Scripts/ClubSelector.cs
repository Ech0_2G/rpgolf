﻿using UnityEngine;
using UnityEngine.UI;

public class ClubSelector : MonoBehaviour {

    GameManager gameManager;
    Image icon;

    void Start() {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        icon = GetComponent<Image>();
        Debug.Log(icon);
        GameEvents.current.onClubChange += UpdateClubImage;
    }

    void OnDestroy() {
        GameEvents.current.onClubChange -= UpdateClubImage;
    }

    void UpdateClubImage() {
        Debug.Log("Club Changed");
        Club club = gameManager.CurrentClub();
        icon.sprite = club.icon;
    }
}