public enum Lie {
    Tee,
    Fairway,
    Rough,
    Sand,
    Green
}