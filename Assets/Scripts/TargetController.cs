﻿using UnityEngine;

public class TargetController : MonoBehaviour {
    private GameManager gameManager;
    public GameObject ball;

    private Ray ray;
    private RaycastHit hit;

    [SerializeField]
    private LayerMask layer;
    private bool targeting = false;

    void Start() {
        hit.point = transform.position;
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        GameEvents.current.onInitiateShotTargeting += InitiateTargeting;
        GameEvents.current.onInitiateShotPrep += InitiateShotPrep;
    }

    void OnDestroy() {
        GameEvents.current.onInitiateShotTargeting -= InitiateTargeting;
        GameEvents.current.onInitiateShotPrep -= InitiateShotPrep;
    }

    void Update() {
        if (!targeting) { return; }
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layer)) {
            transform.position = FindPointOnLine(hit.point) + Vector3.up * 0.1f;
        }
    }

    void InitiateTargeting() => targeting = true;
    void InitiateShotPrep() => targeting = false;

    Vector3 FindPointOnLine(Vector3 point) {
        float distance = gameManager.CurrentClub().distance;
        Vector3 line = Vector3.Normalize(point - ball.transform.position);
        Vector3 targetPos = ball.transform.position + (distance * line);
        return targetPos;
    }
}