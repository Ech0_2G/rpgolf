﻿using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LaunchArcRenderer : MonoBehaviour {

    [SerializeField]
    private GameObject ball;
    private BallLauncher ballLauncher;
    [SerializeField]
    private Transform target;

    [SerializeField]
    [Range(1, 1000)]
    private int resolution = 10;
    private LineRenderer line;

    private float radianAngle;
    private float gravity;
    private bool targeting = false;

    void Start() {
        line = GetComponent<LineRenderer>();
        ballLauncher = ball.GetComponent<BallLauncher>();
        gravity = Physics.gravity.magnitude;
        GameEvents.current.onInitiateShotTargeting += InitiateTargeting;
        GameEvents.current.onInitiateShot += InitiateShot;
    }

    void OnDestroy() {
        GameEvents.current.onInitiateShotTargeting -= InitiateTargeting;
        GameEvents.current.onInitiateShot -= InitiateShot;
    }

    void LateUpdate() {
        if (!targeting) { 
            Unvisualize(); 
            return;
        }
        Visualize();
    }

    void Visualize() {
        Vector3 velocity = ballLauncher.CalculateVelocity(target.position);

        line.positionCount = resolution;
        line.SetPositions(CalculateArcArray(velocity));
    }

    void Unvisualize() {
        line.positionCount = 0;
    }

    Vector3[] CalculateArcArray(Vector3 velocity) {
        Vector3[] arcArray = new Vector3[resolution];

        float time;
        float timeToTarget = CalculateTimeToTarget(velocity, Vector3.Distance(ball.transform.position, target.transform.position));

        for (int i = 0; i < resolution; i++) {
            time = (float) i / (float) resolution * timeToTarget;
            arcArray[i] = CalculatePositionInTime(velocity, time);
        }

        return arcArray;
    }

    float CalculateTimeToTarget(Vector3 velocity, float distance) {
        return distance * (2f / velocity.magnitude);
    }

    Vector3 CalculatePositionInTime(Vector3 velocity, float time) {
        return Physics.gravity * time * time * 0.5f + velocity * time + ball.transform.position;
    }

    void InitiateTargeting() => targeting = true;
    void InitiateShot(float _swing, float _accuracy) => targeting = false;
}