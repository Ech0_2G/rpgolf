﻿using UnityEngine;

public class CameraController : MonoBehaviour {
    [SerializeField]
    private Transform shotTarget;
    [SerializeField]
    private Transform ballTarget;
    [SerializeField]
    private Transform holeTarget;
    [SerializeField]
    private float speed;
    [SerializeField]
    private float allowanceDistance;

    private bool targeting = false;

    void Start() {
        GameEvents.current.onInitiateShotTargeting += InitiateTargeting;
        GameEvents.current.onInitiateShotPrep += InitiateShotPrep;
    }

    void FixedUpdate() {
        Vector3 desiredPosition = (targeting) ? GetTargetingPosition() : GetShotPosition();
        transform.position = Vector3.MoveTowards(transform.position, desiredPosition, speed * Time.deltaTime);

        transform.LookAt(new Vector3(holeTarget.position.x, transform.position.y, holeTarget.position.z));
    }

    Vector3 GetTargetingPosition() {
        Vector3 desiredPosition = shotTarget.position;
        if (Mathf.Abs(Vector3.Distance(transform.position, desiredPosition)) > allowanceDistance) {
            var line = Vector3.Normalize(desiredPosition - transform.position);
            desiredPosition = (transform.position + (allowanceDistance * line));
        } else {
            desiredPosition = transform.position;
        }
        return desiredPosition;
    }

    Vector3 GetShotPosition() => ballTarget.position;

    void OnDestroy() {
        GameEvents.current.onInitiateShotTargeting -= InitiateTargeting;
        GameEvents.current.onInitiateShotPrep -= InitiateShotPrep;
    }

    void InitiateTargeting() => targeting = true;
    void InitiateShotPrep() => targeting = false;
}