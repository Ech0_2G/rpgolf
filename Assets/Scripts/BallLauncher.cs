﻿using UnityEngine;

public class BallLauncher : MonoBehaviour {
    private GameManager gameManager;
    [SerializeField]
    private Transform target;

    private Vector3 initialPosition;

    [SerializeField]
    private float launchAngle = 21;

    [SerializeField]
    [Header("Ground Type Layers")]
    private LayerMask fairwayLayer;

    [SerializeField]
    private LayerMask roughLayer, sandLayer, greenLayer;

    [SerializeField]
    [Header("Ground Type Drags")]
    private float fairwayDrag;

    [SerializeField]
    private float roughDrag, sandDrag, greenDrag;

    private float power = 0f;
    private float gravity, angle, distance, yOffset, initialVelocity, angleBetweenObjects;
    private Vector3 p, planarTarget, planarPosition, velocity, finalVelocity;
    private bool swing = false;
    private bool dragSet = false;

    Rigidbody body;

    void Start() {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        body = GetComponent<Rigidbody>();
        gravity = Physics.gravity.magnitude;
        initialPosition = transform.position;
        GameEvents.current.onInitiateShot += Swing;
        GameEvents.current.onResetShot += Reset;
        GameEvents.current.onHoleComplete += HoleComplete;
    }

    void OnDestroy() {
        GameEvents.current.onInitiateShot -= Swing;
        GameEvents.current.onResetShot -= Reset;
    }

    public void Reset() {
        body.velocity = Vector3.zero;
        body.angularVelocity = Vector3.zero;
        transform.position = initialPosition;
    }

    public void HoleComplete() {
        Debug.Log("\n===== YOU DID IT =============================\n");
        GameEvents.current.ResetShot();
    }

    public void Swing(float SwingPercentage, float accuracy) {
        p = target.position + target.right * 5f * accuracy;
        finalVelocity = CalculateVelocity(p);
        finalVelocity *= SwingPercentage;
        body.AddForce(finalVelocity * body.mass, ForceMode.Impulse);
        swing = false;
    }

    public Vector3 CalculateVelocity(Vector3 targetPosition) {
        angle = gameManager.CurrentClub().launchAngle * Mathf.Deg2Rad;
        planarTarget = new Vector3(targetPosition.x, 0, targetPosition.z);
        planarPosition = new Vector3(transform.position.x, 0, transform.position.z);
        distance = Vector3.Distance(planarTarget, planarPosition);
        yOffset = transform.position.y - targetPosition.y;
        initialVelocity = (1 / Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * gravity * Mathf.Pow(distance, 2)) / (distance * Mathf.Tan(angle) + yOffset));
        velocity = new Vector3(0, initialVelocity * Mathf.Sin(angle), initialVelocity * Mathf.Cos(angle));
        angleBetweenObjects = Vector3.Angle(Vector3.forward, planarTarget - planarPosition) * (targetPosition.x > transform.position.x ? 1 : -1);
        return Quaternion.AngleAxis(angleBetweenObjects, Vector3.up) * velocity;
    }

    void OnCollisionEnter(Collision other) {
       SetDrag(other.gameObject);
    }

    void OnCollisionStay(Collision other) {
       SetDrag(other.gameObject);
    }

    void OnCollisionExit(Collision other) {
        body.angularDrag = 0f;
        dragSet = false;
    }

    void SetDrag(GameObject other) {
       if (dragSet) return; 

        if (LayerMaskMatches(other.gameObject, fairwayLayer)) {
            body.angularDrag = fairwayDrag;
            dragSet = true;
        } else if (LayerMaskMatches(other.gameObject, greenLayer)) {
            body.angularDrag = greenDrag;
            dragSet = true;
        } else if (LayerMaskMatches(other.gameObject, roughLayer)) {
            body.angularDrag = roughDrag;
            dragSet = true;
        } else if (LayerMaskMatches(other.gameObject, sandLayer)) {
            body.angularDrag = sandDrag;
            body.velocity = body.velocity.normalized;
            dragSet = true;
        }
    }

    bool LayerMaskMatches(GameObject collisionObject, LayerMask layer) {
        return ((1 << collisionObject.layer) & layer.value) != 0;
    }
}