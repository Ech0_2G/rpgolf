﻿using UnityEngine;
using UnityEngine.UI;
using Utilities;

public class ShotMeter : MonoBehaviour {
    [Header("Marker Data")]
    [Range(300f, 1500f)]
    [SerializeField]
    private float MarkerSpeed;
    
    [Header("Meter Fill Minimum")]
    [SerializeField]
    private float ShotMeterFillMin;

    [Header("Accuracy Positions")]
    [SerializeField]
    private float AccuracyDiffMax;
    [SerializeField]
    private float AccuracyPositionPerfect;

    [Header("Shot Positions")]
    [SerializeField]
    private float MarkerFudgeAmount;
    [SerializeField]
    private float MarkerPositionMin;
    [SerializeField]
    private float MarkerPositionY;
    [SerializeField]
    private float MarkerPositionZero;
    [SerializeField]
    private float MarkerPositionPerfect;
    [SerializeField]
    private float MarkerPositionMax;
    [SerializeField]
    private float MeterBorderWidth;

    [Header("UI Elements")]
    [SerializeField]
    private Image ShotMeterBg;

    [SerializeField]
    private Image ShotMarker;

    private ShotState State;

    private float MarkerPosition;
    private float FillAmount;
    private float Power;
    private float Accuracy;
    private float AccuracyDiff;
    private float AccuracyModifier;
    private float MarkerSpeedModifier = 1f;
    private float MarkerMovement = 1f;
    private bool left = true, gettingPower = false, gettingAccuracy = false;

    void Start() {
        ShotMeterBg.fillAmount = ShotMeterFillMin;
        MarkerPosition = MarkerPositionZero;
        ShotMarker.rectTransform.localPosition = new Vector3(MarkerPositionZero, MarkerPositionY, 0f);

        GameEvents.current.onInitiateShotPower += InititateShotPower;
        GameEvents.current.onGetShotPower += GetPower;
        GameEvents.current.onInitiateShotAccuracy += InititateShotAccuracy;
        GameEvents.current.onGetShotAccuracy += GetAccuracy;
    }

    void OnDestroy() {
        GameEvents.current.onInitiateShotPower -= InititateShotPower;
        GameEvents.current.onGetShotPower -= GetPower;
        GameEvents.current.onInitiateShotAccuracy -= InititateShotAccuracy;
        GameEvents.current.onGetShotAccuracy -= GetAccuracy;
    }

    void FixedUpdate() {
        if (MarkerPosition - MarkerSpeed * Time.fixedDeltaTime < MarkerPositionMax + MeterBorderWidth) {
            left = false;
        } else if (MarkerPosition + MarkerSpeed * Time.fixedDeltaTime > MarkerPositionMin - MeterBorderWidth) {
            left = true;
        }

        if (gettingPower) {
                MarkerSpeedModifier = (left) ? 1f: -1f;
                MarkerMovement = Mathf.Floor(MarkerSpeed * MarkerSpeedModifier * Time.fixedDeltaTime);
                MarkerMovement = Mathf.Round(MarkerMovement / 5f) * 5f;
                MarkerPosition -= MarkerMovement;
                FillAmount = (MarkerPosition - MarkerPositionMin) / (MarkerPositionMax - MarkerPositionMin);
                ShotMeterBg.fillAmount = FillAmount;
        } else if (gettingAccuracy) {
                MarkerSpeedModifier = (Power > 1.05f) ? 2f: 1f;
                if (MarkerPosition + Mathf.Floor(MarkerSpeed * MarkerSpeedModifier * Time.fixedDeltaTime) > MarkerPositionMin - MeterBorderWidth) {
                    MarkerPosition = MarkerPositionMin - MeterBorderWidth;
                    State = ShotState.Targeting;
                }
                ShotMeterBg.fillAmount = FillAmount;
                MarkerPosition += Mathf.Floor(MarkerSpeed * MarkerSpeedModifier * Time.fixedDeltaTime);
        }
        ShotMarker.rectTransform.localPosition = new Vector3(MarkerPosition, MarkerPositionY, 0f);
    }

    void InititateShotPower() {
        gettingPower = true;
    }

    void InititateShotAccuracy() {
        gettingAccuracy = true;
    }

    void Swing() {
        GameEvents.current.InitiateShot(Power, Accuracy);
    }

    void GetPower() {
        gettingPower = false;
        if (MarkerPosition <= MarkerPositionPerfect + MarkerFudgeAmount && MarkerPosition >= MarkerPositionPerfect - MarkerFudgeAmount) {
            MarkerPosition = MarkerPositionPerfect;
        }
        Power = (MarkerPosition - MarkerPositionZero) / (MarkerPositionPerfect - MarkerPositionZero);
    }

    void GetAccuracy() {
        gettingAccuracy = false;
        if (MarkerPosition <= MarkerPositionZero + MarkerFudgeAmount && MarkerPosition >= MarkerPositionZero - MarkerFudgeAmount) {
            MarkerPosition = MarkerPositionZero;
        }

        AccuracyDiff = Mathf.Abs(AccuracyPositionPerfect - MarkerPosition);
        AccuracyModifier = (AccuracyDiff > AccuracyDiffMax) ? 5f: 1f;

        Accuracy = AccuracyDiff / AccuracyDiffMax;
        Accuracy *= AccuracyModifier;
        Accuracy *= (MarkerPosition < AccuracyPositionPerfect) ? -1f: 1f;
        Swing();
    }
}